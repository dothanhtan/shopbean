<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    protected Brand $brand;
    protected Category $category;
    protected Order $order;
    protected Product $product;
    protected User $user;

    /**
     * Create a new controller instance.
     *
     * @param Brand $brand
     * @param Category $category
     * @param Order $order
     * @param Product $product
     * @param User $user
     */
    public function __construct(Brand $brand, Category $category, Order $order, Product $product, User $user)
    {
        $this->brand = $brand;
        $this->category = $category;
        $this->order = $order;
        $this->product = $product;
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todayOrder = Carbon::now()->format('Y-m-d');
        $monthOrder = Carbon::now()->format('m');
        $yearOrder = Carbon::now()->format('Y');

        $totalDateOrder = $this->order->whereDate('created_at', $todayOrder)->count();
        $totalMonthOrder = $this->order->whereMonth('created_at', $monthOrder)->count();
        $totalYearOrder = $this->order->whereYear('created_at', $yearOrder)->count();

        $totalRoleUser = $this->user->where('role', '0')->count();
        $totalRoleAdmin = $this->user->where('role', '1')->count();
        $totalRoleOther = $this->user->where('role', '2')->count();

        $totalBrands = $this->brand->count();
        $totalCategories = $this->category->count();
        $totalOrders = $this->order->count();
        $totalProducts = $this->product->count();
        $totalUsers = $this->user->count();
        return view('admin.dashboard.index', compact('totalDateOrder', 'totalMonthOrder', 'totalYearOrder', 'totalRoleUser', 'totalRoleAdmin', 'totalRoleOther', 'totalBrands', 'totalCategories', 'totalOrders', 'totalProducts', 'totalUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
