<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\InvoiceOrderMail;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * Create a new controller instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $orders = $this->order->latest('created_at')->paginate(5);
        $todayDate = Carbon::now()->format('Y-m-d');
        $orders = $this->order->when($request->date != null, function ($query) use ($request) {
            return $query->whereDate('created_at', $request->date);
        }, function ($query) use ($todayDate) {
            return $query->whereDate('created_at', $todayDate);
        })->when($request->status != null, function ($query) use ($request) {
            return $query->where('status_message', $request->status);
        })->paginate(5);
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $order = $this->order->findOrFail($id);
        if ($order) {
            return view('admin.orders.show', compact('order'));
        } else {
            return redirect()->back()->with('message', 'Order ID Not Found');
        }
    }

    /**
     * Show the page for download the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function display(int $id)
    {
        $order = $this->order->findOrFail($id);
        return view('admin.invoice.index', compact('order'));
    }

    public function generate(int $id)
    {
        $order = $this->order->findOrFail($id);
        $data = ['order' => $order];
        $pdf = Pdf::loadView('admin.invoice.index', $data);
        $todayDate = Carbon::now()->format('d-m-Y');
        return $pdf->download('invoice-'.$order->id.'-'.$todayDate.'.pdf');
    }

    public function send(int $id)
    {
        $order = $this->order->findOrFail($id);
        try {
            Mail::to("$order->email")->send(new InvoiceOrderMail($order));
            return redirect()->route('orders.show', $id)->with('message', 'Invoice Mail has been sent to ' .$order->email);
        } catch (Exception $exception) {
            return redirect()->route('orders.show', $id)->with('message', 'Something Went Wrong!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $order = $this->order->findOrFail($id);
        if ($order) {
            $order->update(['status_message' => $request->order_status]);
            return redirect()->route('orders.show', $id)->with('message', 'Order Status Updated');
        } else {
            return redirect()->route('orders.show', $id)->with('message', 'Something Went Wrong!');
        }
    }
}
