<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Color\ColorFormRequest;
use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    /**
     * @var Color $color
     */
    protected Color $color;

    /**
     * Create a new controller instance.
     *
     * @param Color $color
     */
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $colors = $this->color->latest('id')->paginate(5);
        return view('admin.colors.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ColorFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColorFormRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['status'] = $request->status ? '1' : '0';
        $color = $this->color->create($validatedData);
        return redirect()->route('colors.index')->with('message', 'Color '.$color->name.' has already created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Color  $color
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
        return view('admin.colors.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColorFormRequest $request, int $id)
    {
        $validatedData = $request->validated();
        $validatedData['status'] = $request->status ? '1' : '0';
        $color = $this->color->findOrFail($id);
        $color->update($validatedData);
        return redirect()->route('colors.index')->with('message', 'Color '.$color->name.' has already updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $color = $this->color->findOrFail($id);
        $color->delete();
        return redirect()->route('colors.index')->with('message', 'This color has already deleted successfully!');
    }
}
