<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductFormRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductImage;
use App\Models\ProductSize;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    /**
     * @var Product $product
     * @var Category $category
     * @var Brand $brand
     * @var Color $color
     * @var Size $size
     * @var ProductImage $productImage
     * @var ProductColor $productColor
     * @var ProductSize $productSize
     */
    protected Product $product;
    protected Category $category;
    protected Brand $brand;
    protected Color $color;
    protected Size $size;
    protected ProductImage $productImage;
    protected ProductColor $productColor;
    protected ProductSize $productSize;

    /**
     * Create a new controller instance.
     *
     * @param Product $product
     * @param Category $category
     * @param Brand $brand
     * @param Color $color
     * @param Size $size
     * @param ProductImage $productImage
     * @param ProductColor $productColor
     * @param ProductSize $productSize
     */
    public function __construct(
        Product $product, Category $category,
        Brand $brand, Color $color, Size $size,
        ProductImage $productImage,
        ProductColor $productColor,
        ProductSize $productSize)
    {
        $this->product = $product;
        $this->category = $category;
        $this->brand = $brand;
        $this->color = $color;
        $this->size = $size;
        $this->productImage = $productImage;
        $this->productColor = $productColor;
        $this->productSize = $productSize;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->product->latest('id')->paginate(5);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->category->all();
        $brands = $this->brand->all();
        $colors = $this->color->where('status', '0')->get();
        $sizes = $this->size->where('status', '0')->get();
        return view('admin.products.create', compact('categories', 'brands', 'colors', 'sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFormRequest $request)
    {
        $dataValidated = $request->validated();
        $category = $this->category->findOrFail($dataValidated['category_id']);
        $product = $category->products()->create([
            'category_id' => $dataValidated['category_id'],
            'name' => $dataValidated['name'],
            'slug' => Str::slug($dataValidated['slug']),
            'brand' => $dataValidated['brand'],
            'short_description' => $dataValidated['short_description'],
            'description' => $dataValidated['description'],
            'original_price' => $dataValidated['original_price'],
            'selling_price' => $dataValidated['selling_price'],
            'quantity' => $dataValidated['quantity'],
            'trending' => $request->trending ? '1' : '0',
            'featured' => $request->featured ? '1' : '0',
            'status' => $request->status ? '1' : '0',
            'meta_title' => $dataValidated['meta_title'],
            'meta_keyword' => $dataValidated['meta_keyword'],
            'meta_description' => $dataValidated['meta_description']
        ]);

        if ($request->hasFile('image')) {
            $uploadPath = 'upload/product/';
            $index = 1;
            foreach ($request->file('image') as $imageFile) {
                $extension = $imageFile->getClientOriginalExtension();
                $filename = time().$index++.'.'.$extension;
                $imageFile->move($uploadPath,$filename);
                $finalImagePathName = $uploadPath.$filename;
                $product->productImages()->create([
                    'product_id' => $product->id,
                    'image' => $finalImagePathName
                ]);
            }
        }

        if ($request->colors) {
            foreach ($request->colors as $key => $color) {
                $product->productColors()->create([
                    'product_id' => $product->id,
                    'color_id' => $color,
                    'quantity' => $request->color_quantity[$key] ?? 0
                ]);
            }
        }

        if ($request->sizes) {
            foreach ($request->sizes as $key => $size) {
                $product->productSizes()->create([
                    'product_id' => $product->id,
                    'size_id' => $size,
                    'quantity' => $request->size_quantity[$key] ?? 0
                ]);
            }
        }
        return redirect()->route('products.index')->with('message', 'Product created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $categories = $this->category->all();
        $brands = $this->brand->all();
        $product = $this->product->findOrFail($id);
        $productColor = $product->productColors->pluck('color_id')->toArray();
        $colors = $this->color->whereNotIn('id', $productColor)->get();
        $productSize = $product->productSizes->pluck('size_id')->toArray();
        $sizes = $this->size->whereNotIn('id', $productSize)->get();
        return view('admin.products.edit', compact('categories', 'brands', 'product', 'colors', 'sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductFormRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFormRequest $request, int $id)
    {
        $dataUpdated = $request->validated();
        $product = $this->category->findOrFail($dataUpdated['category_id'])->products()->where('id', $id)->first();
        if ($product) {
            $product->update([
                'category_id' => $dataUpdated['category_id'],
                'name' => $dataUpdated['name'],
                'slug' => Str::slug($dataUpdated['slug']),
                'brand' => $dataUpdated['brand'],
                'short_description' => $dataUpdated['short_description'],
                'description' => $dataUpdated['description'],
                'original_price' => $dataUpdated['original_price'],
                'selling_price' => $dataUpdated['selling_price'],
                'quantity' => $dataUpdated['quantity'],
                'trending' => $request->trending ? '1' : '0',
                'featured' => $request->featured ? '1' : '0',
                'status' => $request->status ? '1' : '0',
                'meta_title' => $dataUpdated['meta_title'],
                'meta_keyword' => $dataUpdated['meta_keyword'],
                'meta_description' => $dataUpdated['meta_description']
            ]);

            if ($request->hasFile('image')) {
                $uploadPath = 'upload/product/';
                $index = 1;
                foreach ($request->file('image') as $imageFile) {
                    $extension = $imageFile->getClientOriginalExtension();
                    $filename = time().$index++.'.'.$extension;
                    $imageFile->move($uploadPath,$filename);
                    $finalImagePathName = $uploadPath.$filename;
                    $product->productImages()->create([
                        'product_id' => $product->id,
                        'image' => $finalImagePathName
                    ]);
                }
            }

            if ($request->colors) {
                foreach ($request->colors as $key => $color) {
                    $product->productColors()->create([
                        'product_id' => $product->id,
                        'color_id' => $color,
                        'quantity' => $request->color_quantity[$key] ?? 0
                    ]);
                }
            }

            if ($request->sizes) {
                foreach ($request->sizes as $key => $size) {
                    $product->productSizes()->create([
                        'product_id' => $product->id,
                        'size_id' => $size,
                        'quantity' => $request->size_quantity[$key] ?? 0
                    ]);
                }
            }

            return redirect()->route('products.index')->with('message', 'Product updated successfully!');
        } else {
            return redirect()->route('products.index')->with('message', 'No such product id found!');
        }
    }

    public function updateColorQuantity(Request $request, int $product_color_id)
    {
        $productColorData = $this->product->findOrFail($request->product_id)->productColors()->where('id', $product_color_id)->first();
        $productColorData->update(['quantity' => $request->product_color_quantity]);
        return response()->json(['message' => 'Product Color Quantity has updated successfully!']);
    }

    public function updateSizeQuantity(Request $request, int $product_size_id)
    {
        $productSizeData = $this->product->findOrFail($request->product_id)->productSizes()->where('id', $product_size_id)->first();
        $productSizeData->update(['quantity' => $request->product_size_quantity]);
        return response()->json(['message' => 'Product Size Quantity has updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $product = $this->product->findOrFail($id);
        if ($product->productImages) {
            foreach ($product->productImages as $item) {
                if (File::exists($item->image)) {
                    File::delete($item->image);
                }
            }
        }
        $product->delete();
        return redirect()->route('products.index')->with('message', 'Product deleted with all its image!');
    }

    public function remove(int $image_id)
    {
        $productImage = $this->productImage->findOrFail($image_id);
        if (File::exists($productImage->image)) {
            File::delete($productImage->image);
        }
        $productImage->delete();
        return back()->with('message', 'Image deleted successfully!');
    }

    public function removeColor(int $product_color_id)
    {
        $productColor = $this->productColor->findOrFail($product_color_id);
        $productColor->delete();
        return response()->json(['message' => 'Product Color has deleted successfully!']);
    }

    public function removeSize(int $product_size_id)
    {
        $productSize = $this->productSize->findOrFail($product_size_id);
        $productSize->delete();
        return response()->json(['message' => 'Product Size has deleted successfully!']);
    }
}
