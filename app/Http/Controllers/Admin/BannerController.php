<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\CreateBannerRequest;
use App\Http\Requests\Banner\UpdateBannerRequest;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
    /**
     * @var Banner $banner
     */
    protected Banner $banner;

    /**
     * Create a new controller instance.
     *
     * @param Banner $banner
     */
    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->banner->latest('id')->paginate(5);
        return view('admin.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateBannerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBannerRequest $request)
    {
        $validatedData = $request->validated();
        $validatedData['status'] = $request->status ? '1' : '0';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time() .'.'. $extension;
            $file->move('upload/banner/', $fileName);
            $validatedData['image'] = $fileName;
        }
        $this->banner->create([
            'title' => $validatedData['title'],
            'description' => $validatedData['description'],
            'image' => $validatedData['image'],
            'status' => $validatedData['status']
        ]);
        return redirect()->route('banners.index')->with('message', 'Banner has already created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('admin.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateBannerRequest  $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBannerRequest $request, int $id)
    {
        $validatedData = $request->validated();
        $banner = $this->banner->findOrFail($id);

        if ($request->hasFile('image')) {
            $path = "upload/banner/$banner->image";
            if (File::exists($path)) {
                File::delete($path);
            }

            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $fileName = time().'.'.$extension;
            $file->move('upload/banner/', $fileName);
            $banner->image = $fileName;
        }

        $banner->title = $validatedData['title'];
        $banner->description = $validatedData['description'];
        $banner->status = $request->status ? '1' : 0;
        $banner->update();

        return redirect()->route('banners.index')->with('message', 'Banner has already updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $banner = $this->banner->findOrFail($id);
        if ($banner->count() > 0) {
            $path = "upload/banner/$banner->image";
            if (File::exists($path)) {
                File::delete($path);
            }
            $banner->delete();
            return redirect()->route('banners.index')->with('message', 'Banner has already deleted successfully!');
        }
        return redirect()->route('banners.index')->with('message', 'Something went wrong!');
    }
}
