<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * Create a new controller instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = $this->order->where('user_id', Auth::id())->orderBy('created_at', 'desc')->paginate(5);
        return view('shopping.order.index', compact('orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $order = $this->order->where('user_id', Auth::id())->where('id', $id)->first();
        if ($order) {
            return view('shopping.order.show', compact('order'));
        } else {
            return redirect()->back()->with('message', 'No Order Found');
        }
    }
}
