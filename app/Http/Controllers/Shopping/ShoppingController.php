<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    /**
     * @var Banner $banner
     */
    protected Banner $banner;

    /**
     * @var Category $category
     */
    protected Category $category;

    /**
     * @var Product $product
     */
    protected Product $product;

    /**
     * @param Banner $banner
     * @param Category $category
     * @param Product $product
     */
    public function __construct(Banner $banner, Category $category, Product $product)
    {
        $this->banner = $banner;
        $this->category = $category;
        $this->product = $product;
    }

    /**
     * Display a listing of the banner.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = $this->banner->where('status', '0')->get();
        $trendingProducts = $this->product->where('trending', '1')->latest()->take(15)->get();
        $newArrivalProducts = $this->product->latest()->take(14)->get();
        $featuredProducts = $this->product->where('featured', '1')->latest()->take(14)->get();
        return view('shopping.home.index', compact('banners', 'trendingProducts', 'newArrivalProducts', 'featuredProducts'));
    }

    public function search(Request $request)
    {
        if ($request->search) {
            $searchProducts = $this->product->where('name', 'LIKE', "%$request->search%")->latest()->paginate(5);
            return view('shopping.page.search', compact('searchProducts'));
        } else {
            return redirect()->back()->with('message', 'No product found');
        }
    }

    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
        $categories = $this->category->where('status', '0')->get();
        return view('shopping.category.index', compact('categories'));
    }

    /**
     * Display a listing of the product belong to category.
     *
     * @return \Illuminate\Http\Response
     */
    public function catalogProduct($slug)
    {
        $category = $this->category->where('slug', $slug)->first();
        if ($category) {
            return view('shopping.product.index', compact('category'));
        } else {
            return redirect()->back();
        }
    }

    public function productDetail(string $category_slug, string $product_slug)
    {
        $category = $this->category->where('slug', $category_slug)->first();
        if ($category) {
            $product = $category->products()->where('slug', $product_slug)->where('status', '0')->first();
            if ($product) {
                return view('shopping.product.detail', compact('category', 'product'));
            } else {
                return redirect()->back();
            }
        } else {
            return redirect()->back();
        }
    }

    public function trendingProduct()
    {
        $trendingProducts = $this->product->where('trending', '1')->latest()->take(15)->get();
        return view('shopping.page.trending', compact('trendingProducts'));
    }

    public function newArrival()
    {
        $newArrivalProducts = $this->product->latest()->take(16)->get();
        return view('shopping.page.arrival', compact('newArrivalProducts'));
    }

    public function featuredProduct()
    {
        $featuredProducts = $this->product->where('featured', '1')->latest()->get();
        return view('shopping.page.featured', compact('featuredProducts'));
    }

    public function thankyou()
    {
        return view('shopping.thankyou.index');
    }
}
