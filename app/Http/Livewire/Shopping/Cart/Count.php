<?php

namespace App\Http\Livewire\Shopping\Cart;

use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Count extends Component
{
    public $cartCount;

    protected $listeners = ['cartAddedOrUpdated' => 'checkCartCount'];

    public function checkCartCount()
    {
        if (Auth::check())
        {
            return $this->cartCount = Cart::where('user_id', Auth::id())->count();
        }
        else
        {
            return $this->cartCount = 0;
        }
    }

    public function render()
    {
        $this->cartCount = $this->checkCartCount();
        return view('livewire.shopping.cart.count', ['cartCount' => $this->cartCount]);
    }
}
