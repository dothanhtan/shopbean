<?php

namespace App\Http\Livewire\Shopping\Cart;

use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Index extends Component
{
    public $cart, $totalPrice = 0;

    public function decrementQuantity(int $cartID)
    {
        $cartData = Cart::where('id', $cartID)->where('user_id', Auth::id())->first();
        if ($cartData) {
            $hasProductColor = $cartData->productColor()->where('id', $cartData->product_color_id)->exists();
            $hasProductSize = $cartData->productSize()->where('id', $cartData->product_size_id)->exists();
            if ($hasProductColor and $hasProductSize)
            {
                $productColor = $cartData->productColor()->where('id', $cartData->product_color_id)->first();
                $productSize = $cartData->productSize()->where('id', $cartData->product_size_id)->first();
                if ($productColor->quantity > $cartData->quantity and $productSize->quantity > $cartData->quantity)
                {
                    $cartData->decrement('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$productColor->quantity.' product color quantity and '.$productSize->quantity.' product size quantity available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }
            elseif ($cartData->productColor()->where('id', $cartData->product_color_id)->exists())
            {
                $productColor = $cartData->productColor()->where('id', $cartData->product_color_id)->first();
                if ($productColor->quantity > $cartData->quantity)
                {
                    $cartData->decrement('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$productColor->quantity.' quantity available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }
            else
            {
                if ($cartData->product->quantity > $cartData->quantity)
                {
                    $cartData->decrement('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$cartData->product->quantity.' Quantity Available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }

        } else {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Something Went Wrong!',
                'type' => 'error',
                'status' => 404
            ]);
        }
    }

    public function incrementQuantity(int $cartID)
    {
        $cartData = Cart::where('id', $cartID)->where('user_id', Auth::id())->first();
        if ($cartData)
        {
            $hasProductColor = $cartData->productColor()->where('id', $cartData->product_color_id)->exists();
            $hasProductSize = $cartData->productSize()->where('id', $cartData->product_size_id)->exists();
            if ($hasProductColor and $hasProductSize)
            {
                $productColor = $cartData->productColor()->where('id', $cartData->product_color_id)->first();
                $productSize = $cartData->productSize()->where('id', $cartData->product_size_id)->first();
                if ($productColor->quantity > $cartData->quantity and $productSize->quantity > $cartData->quantity)
                {
                    $cartData->increment('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$productColor->quantity.' product color quantity and '.$productSize->quantity.' product size quantity available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }
            elseif ($cartData->productColor()->where('id', $cartData->product_color_id)->exists())
            {
                $productColor = $cartData->productColor()->where('id', $cartData->product_color_id)->first();
                if ($productColor->quantity > $cartData->quantity)
                {
                    $cartData->increment('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$productColor->quantity.' quantity available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }
            else
            {
                if ($cartData->product->quantity > $cartData->quantity)
                {
                    $cartData->increment('quantity');
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Quantity Updated',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
                else
                {
                    $this->dispatchBrowserEvent('message', [
                        'text' => 'Only '.$cartData->product->quantity.' Quantity Available',
                        'type' => 'success',
                        'status' => 200
                    ]);
                }
            }
        }
        else
        {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Something Went Wrong!',
                'type' => 'error',
                'status' => 404
            ]);
        }
    }

    public function removeCartItem(int $cartID)
    {
        $cartItem = Cart::where('user_id', Auth::id())->where('id', $cartID)->first();
        if ($cartItem) {
            $cartItem->delete();
            $this->emit('cartAddedOrUpdated');
            $this->dispatchBrowserEvent('message', [
                'text' => 'Cart Item has already deleted successfully!',
                'type' => 'success',
                'status' => 200
            ]);
        } else {
            $this->dispatchBrowserEvent('message', [
                'text' => 'Something Went Wrong!',
                'type' => 'error',
                'status' => 500
            ]);
        }
    }

    public function render()
    {
        $this->cart = Cart::where('user_id', Auth::id())->get();
        return view('livewire.shopping.cart.index', ['cart' => $this->cart]);
    }
}
