<?php

namespace App\Http\Livewire\Shopping\Product;

use App\Models\Product;
use Livewire\Component;

class Index extends Component
{
    public $products, $category, $brandInput = [], $priceInput;

    protected $queryString = [
        'brandInput' => ['except' => '', 'as' => 'brand'],
        'priceInput' => ['except' => '', 'as' => 'price'],
    ];

    public function mount($category)
    {
        $this->category = $category;
    }
    public function render()
    {
        $this->products = Product::where('category_id', $this->category->id)
            ->when($this->brandInput, fn($query) => $query->whereIn('brand', $this->brandInput))
            ->when($this->priceInput, fn($query) => $query->when($this->priceInput == 'low-to-high', fn($q)
            => $q->orderBy('selling_price', 'ASC'))->when($this->priceInput == 'high-to-low', fn($q)
            => $q->orderBy('selling_price', 'DESC')))->where('status', '0')->get();
        return view('livewire.shopping.product.index', [
            'products' => $this->products,
            'category' => $this->category
        ]);
    }
}
