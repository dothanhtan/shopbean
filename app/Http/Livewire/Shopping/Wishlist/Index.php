<?php

namespace App\Http\Livewire\Shopping\Wishlist;

use App\Models\Wishlist;
use Livewire\Component;

class Index extends Component
{
    public function removeWishlistItem(int $wishlistID)
    {
        Wishlist::where('user_id', auth()->id())->where('id', $wishlistID)->delete();
        $this->emit('wishlistAddedOrUpdated');
        $this->dispatchBrowserEvent('message', [
            'text' => 'Wishlist Item has already deleted successfully!',
            'type' => 'success',
            'status' => 200
        ]);
    }

    public function render()
    {
        $wishlist = Wishlist::where('user_id', auth()->id())->get();
        return view('livewire.shopping.wishlist.index', compact('wishlist'));
    }
}
