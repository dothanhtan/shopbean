<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if (isset($this->changePassword)) {
            return [
                'name' => ['required', 'string', 'max:255'],
                'role' => ['required', 'integer'],
                'password' => ['required', 'string', 'min:8'],
                'password_confirmation' => ['required', 'same:password'],
            ];
        } else {
            return [
                'name' => ['required', 'string', 'max:255'],
                'role' => ['required', 'integer'],
            ];
        }
    }
}
