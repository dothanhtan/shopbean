<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('brand')->nullable();
            $table->mediumText('short_description')->nullable();
            $table->longText('description')->nullable();

            $table->unsignedInteger('quantity');
            $table->decimal('original_price');
            $table->decimal('selling_price');
            $table->tinyInteger('trending')->default('0')->comment('1 => trending, 0 => not trending');
            $table->tinyInteger('featured')->default('0')->comment('1 => featured, 0 => not featured');
            $table->tinyInteger('status')->default('0')->comment('1 => hidden, 0 => visible');

            $table->string('meta_title')->nullable();
            $table->text('meta_keyword')->nullable();
            $table->mediumText('meta_description')->nullable();

            $table->foreignIdFor(Category::class)->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
