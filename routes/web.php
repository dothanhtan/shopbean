<?php

use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ColorController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SizeController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Shopping\CartController;
use App\Http\Controllers\Shopping\CheckoutController;
use App\Http\Controllers\Shopping\OrderController as SOC;
use App\Http\Controllers\Shopping\ShoppingController;
use App\Http\Controllers\Shopping\WishlistController;
use App\Http\Controllers\Shopping\UserController as SUC;
use App\Http\Livewire\Admin\Brand\Index;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (App::environment('production')) {
    URL::forceScheme('https');
}

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::controller(ShoppingController::class)->group(function () {
    Route::get('/', 'index')->name('shopping');
    Route::get('/search', 'search')->name('search');
    Route::get('/category', 'category')->name('category');
    Route::get('/category/{slug}', 'catalogProduct')->name('category.product');
    Route::get('/product/{category}/{slug}', 'productDetail')->name('product.detail');
    Route::get('/trending-product', 'trendingProduct')->name('trending.product');
    Route::get('/new-arrival', 'newArrival')->name('new.arrival');
    Route::get('/featured-product', 'featuredProduct')->name('featured.product');
    Route::get('/thank-you', 'thankyou')->name('thank.you');
});

Route::middleware('auth')->group(function () {
    Route::get('/wishlist', [WishlistController::class, 'index'])->name('wishlist');
    Route::get('/cart', [CartController::class, 'index'])->name('cart');
    Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');
    Route::get('/order', [SOC::class, 'index'])->name('order.index');
    Route::get('/order/{order}', [SOC::class, 'show'])->name('order.show');
    Route::get('/profile', [SUC::class, 'index'])->name('profile.index');
    Route::post('/profile', [SUC::class, 'store'])->name('profile.store');
    Route::get('/password', [SUC::class, 'show'])->name('password.create');
    Route::post('/password', [SUC::class, 'update'])->name('password.change');
});

Route::prefix('/admin')->middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('/brands', Index::class)->name('brands.index');

    Route::prefix('/configs')->controller(ConfigController::class)->name('configs.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
    });

    Route::prefix('/orders')->controller(OrderController::class)->name('orders.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/{order}', 'show')->name('show');
        Route::put('/{order}', 'update')->name('update');
        Route::get('/invoice/show/{id}', 'display')->name('invoice.display');
        Route::get('/invoice/generate/{id}', 'generate')->name('invoice.generate');
        Route::get('/invoice/send/{id}', 'send')->name('invoice.send');
    });

    Route::prefix('/banners')->controller(BannerController::class)->name('banners.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{banner}/edit', 'edit')->name('edit');
        Route::put('/{banner}/update', 'update')->name('update');
        Route::get('/{banner}/delete', 'destroy')->name('destroy');
    });

    Route::prefix('/categories')->controller(CategoryController::class)->name('categories.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{category}/edit', 'edit')->name('edit');
        Route::put('/{category}/update', 'update')->name('update');
    });

    Route::prefix('/products')->controller(ProductController::class)->name('products.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{product}/edit', 'edit')->name('edit');
        Route::put('/{product}/update', 'update')->name('update');
        Route::post('/{product}/color', 'updateColorQuantity')->name('updateColorQuantity');
        Route::post('/{product}/size', 'updateSizeQuantity')->name('updateSizeQuantity');
        Route::get('/{product}/color/delete', 'removeColor')->name('removeColor');
        Route::get('/{product}/size/delete', 'removeSize')->name('removeSize');
        Route::get('/{product}/image', 'remove')->name('remove');
        Route::get('/{product}/delete', 'destroy')->name('destroy');
    });

    Route::prefix('/colors')->controller(ColorController::class)->name('colors.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{color}/edit', 'edit')->name('edit');
        Route::put('/{color}/update', 'update')->name('update');
        Route::get('/{color}/delete', 'destroy')->name('destroy');
    });

    Route::prefix('/sizes')->controller(SizeController::class)->name('sizes.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{size}/edit', 'edit')->name('edit');
        Route::put('/{size}/update', 'update')->name('update');
        Route::get('/{size}/delete', 'destroy')->name('destroy');
    });

    Route::prefix('/users')->controller(UserController::class)->name('users.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{user}/edit', 'edit')->name('edit');
        Route::put('/{user}/update', 'update')->name('update');
        Route::get('/{user}/delete', 'destroy')->name('destroy');
    });
});
