@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
    <!-- content-wrapper start -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>

            <div class="col-md-12 grid-margin">
                <div class="me-md-3 me-xl-5">
                    <h2>Dashboard</h2>
                    <p class="mb-md-0">Your analytics dashboard template.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="card bg-success text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Orders</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalOrders}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('orders.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-primary text-white text-center mb-3">
                    <div class="card-header">
                        <label>Today Orders</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalDateOrder}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('orders.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info text-white text-center mb-3">
                    <div class="card-header">
                        <label>Month Orders</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalMonthOrder}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('orders.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-danger text-white text-center mb-3">
                    <div class="card-header">
                        <label>Year Orders</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalYearOrder}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('orders.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="card bg-success text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Users</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalUsers}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('users.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-primary text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Role Admin</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalRoleAdmin}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('users.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Role User</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalRoleUser}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('users.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-warning text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Role Other</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalRoleOther}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('users.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="card bg-success text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Brands</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalBrands}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('brands.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-primary text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Categories</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalCategories}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('categories.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card bg-info text-white text-center mb-3">
                    <div class="card-header">
                        <label>Total Products</label>
                    </div>
                    <div class="card-body">
                        <h1>{{$totalProducts}}</h1>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('products.index')}}" class="text-white">view</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper end -->

    <!-- partial:partials -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
        </div>
    </footer>
    <!-- partial -->
</div>
@endsection
