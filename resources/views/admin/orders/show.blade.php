@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-md-12">
                    @if(session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center mb-0">
                                <span class="fw-bold">Show Order Detail</span>
                                <span>
                                    <a href="{{route('orders.invoice.send', $order->id)}}" class="btn btn-sm btn-success">SEND INVOICE VIA MAIL</a>
                                    <a href="{{route('orders.invoice.generate', $order->id)}}" class="btn btn-sm btn-primary">DOWNLOAD INVOICE</a>
                                    <a href="{{route('orders.invoice.display', $order->id)}}" target="_blank" class="btn btn-sm btn-info">VIEW INVOICE</a>
                                    <a href="{{route('orders.index')}}" class="btn btn-sm btn-secondary">BACK</a>
                                </span>
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Order Detail</h5>
                                    <div class="border-bottom my-3"></div>

                                    <h6>Tracking ID/NO: {{$order->tracking_no}}</h6>
                                    <h6>Ordered Date: {{$order->created_at->format('d-m-Y H:i A')}}</h6>
                                    <h6>Payment Mode: {{$order->payment_mode}}</h6>
                                    <h6 class="border p-2 text-success">Status Message: <span class="text-uppercase">{{$order->status_message}}</span></h6>
                                </div>

                                <div class="col-md-6">
                                    <h5>User Detail</h5>
                                    <div class="border-bottom my-3"></div>

                                    <h6>Full Name: {{$order->name}}</h6>
                                    <h6>Email: {{$order->email}}</h6>
                                    <h6>Phone Number: {{$order->phone}}</h6>
                                    <h6>Address: {{$order->address}}</h6>
                                    <h6>Pin Code: {{$order->pin_code}}</h6>
                                </div>

                                <div class="col-md-12 mt-4">
                                    <h5>Item Detail</h5>
                                    <div class="border-bottom my-3"></div>

                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Item ID</th>
                                                <th>Image</th>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            @php($totalPrice = 0)
                                            <tbody>
                                            @foreach($order->orderItems as $item)
                                                <tr>
                                                    <td style="width: 10%" class="align-middle">
                                                        {{$item->id}}
                                                    </td>
                                                    <td style="width: 10%">
                                                        <img src="{{asset($item->product->productImages[0]->image)}}" style="width: 50px; height: 50px" alt="{{$item->product->name}}">
                                                    </td>
                                                    <td class="align-middle">
                                                        {{$item->product->name}}
                                                        @if($item->productColor and $item->productColor->color)
                                                            <span> - Color: {{$item->productColor->color->name}}</span>
                                                        @endif
                                                        @if($item->productSize and $item->productSize->size)
                                                            <span> - Size: {{$item->productSize->size->sign}}</span>
                                                        @endif
                                                    </td>
                                                    <td style="width: 10%" class="align-middle">
                                                        ${{$item->price}}
                                                    </td>
                                                    <td style="width: 10%" class="align-middle">
                                                        {{$item->quantity}}
                                                    </td>
                                                    <td style="width: 10%" class="align-middle fw-bold">
                                                        ${{number_format($item->price * $item->quantity, 2)}}
                                                    </td>
                                                    @php($totalPrice += $item->price * $item->quantity)
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="5" class="fw-bold">Total Amount:</td>
                                                <td colspan="1" class="fw-bold">${{number_format($totalPrice, 2)}}</td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4>Order Process (Order Status Updates)</h4>
                            <div class="border-bottom my-3"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('orders.update', $order->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group">
                                            <label for="order_status">Update Order Status</label>
                                            <div class="input-group">
                                                <select id="order_status" name="order_status" class="form-control form-control-sm">
                                                    <option value="">Select All Status</option>
                                                    <option value="in progress" @selected(Request::get('status') == 'in progress')>In Progress</option>
                                                    <option value="completed" @selected(Request::get('status') == 'completed')>Completed</option>
                                                    <option value="pending" @selected(Request::get('status') == 'pending')>Pending</option>
                                                    <option value="cancelled" @selected(Request::get('status') == 'cancelled')>Cancelled</option>
                                                    <option value="out_for_delivery" @selected(Request::get('status') == 'out_for_delivery')>Out for delivery</option>
                                                </select>
                                                <button type="submit" class="btn btn-sm btn-info">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-6">
                                    <h4 style="margin-top: 36px">Current Order Status: <span class="text-uppercase">{{$order->status_message}}</span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
