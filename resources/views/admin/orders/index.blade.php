@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-uppercase fw-bold mt-2">Order</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{route('orders.index')}}" method="GET">
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-3 form-group">
                                        <input type="date" id="date" name="date" value="{{Request::get('date') ?? date('Y-m-d')}}" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <select id="status" name="status" class="form-control form-control-sm">
                                            <option value="">Select All Status</option>
                                            <option value="in progress" @selected(Request::get('status') == 'in progress')>In Progress</option>
                                            <option value="completed" @selected(Request::get('status') == 'completed')>Completed</option>
                                            <option value="pending" @selected(Request::get('status') == 'pending')>Pending</option>
                                            <option value="cancelled" @selected(Request::get('status') == 'cancelled')>Cancelled</option>
                                            <option value="out_for_delivery" @selected(Request::get('status') == 'out_for_delivery')>Out for delivery</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 form-group">
                                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Tracking No</th>
                                            <th>Username</th>
                                            <th>Payment Mode</th>
                                            <th>Ordered Date</th>
                                            <th>Status Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($orders as $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->tracking_no}}</td>
                                            <td>{{$order->name}}</td>
                                            <td>{{$order->payment_mode}}</td>
                                            <td>{{$order->created_at->format('d-m-Y')}}</td>
                                            <td>{{$order->status_message}}</td>
                                            <td><a href="{{route('orders.show', $order->id)}}" class="btn btn-sm btn-outline-info">View</a></td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="7">No order available</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            <div class="mt-4">
                                {{$orders->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
            <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
        </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
            <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
        </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
