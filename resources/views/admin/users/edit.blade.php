@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span class="text-uppercase fw-bold">Edit User</span>
                                <a href="{{route('users.index')}}" class="btn btn-outline-secondary">BACK</a>
                            </h3>
                        </div>
                        <form method="POST" enctype="multipart/form-data" action="{{route('users.update', $user->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="name">Name</label>
                                        <input class="form-control form-control-sm" id="name" name="name" value="{{old('name') ?? $user->name}}" autofocus>
                                        @error('name') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control form-control-sm" id="email" name="email" value="{{old('email') ?? $user->email}}" autofocus readonly>
                                        @error('email') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="phone">Phone</label>
                                        <input type="tel" class="form-control form-control-sm" id="phone" name="phone" value="{{old('phone') ?? $user->phone}}" autofocus readonly>
                                        @error('phone') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="role">Role</label>
                                        <select name="role" id="role" class="form-control form-control-sm">
                                            <option value="">Select Role</option>
                                            <option value="0" @selected((old('role') ?? $user->role) == 0)>User</option>
                                            <option value="1" @selected((old('role') ?? $user->role) == 1)>Admin</option>
                                            <option value="2" @selected((old('role') ?? $user->role) == 2)>Other</option>
                                        </select>
                                        @error('role') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group mb-3">
                                        <input type="checkbox" id="changePassword" name="changePassword" class="form-check-input">
                                        <label for="changePassword">Change Password</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control form-control-sm password" id="password" name="password" value="{{old('password')}}" disabled>
                                        @error('password') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="re_password">Confirm Password</label>
                                        <input type="password" class="form-control form-control-sm password" id="re_password" name="password_confirmation" value="{{old('password_confirmation')}}" disabled>
                                        @error('password_confirmation') <small class="text-danger">{{$message}}</small> @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">UPDATE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                    <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
                </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                    <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
                </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection

@section('script')
    <script>
        $('#changePassword').change(function() {
            if ($(this).is(":checked")) {
                $(".password").removeAttr('disabled');
            } else {
                $(".password").attr('disabled', '');
            }
        });
    </script>
@endsection
