@extends('admin.layouts.app')
@section('content')
    <div class="main-panel">
        <!-- content-wrapper start -->
        <div class="content-wrapper">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="d-flex justify-content-between align-items-center">
                                <span class="text-uppercase">Edit Category</span>
                                <a href="{{route('categories.index')}}" class="btn btn-outline-secondary">BACK</a>
                            </h3>
                        </div>
                        <form method="POST" enctype="multipart/form-data" action="{{route('categories.update', $category->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6 form-group mb-3">
                                        <label for="name">Name</label>
                                        <input class="form-control" id="name" name="name" value="{{old('name') ?? $category->name}}" autofocus>
                                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="slug">Slug</label>
                                        <input class="form-control" id="slug" name="slug" value="{{old('slug') ?? $category->slug}}" autofocus>
                                        @error('slug') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-12 form-group mb-3">
                                        <label for="description">Description</label>
                                        <textarea class="form-control" id="description" name="description" rows="3">{{old('description') ?? $category->description}}</textarea>
                                        @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="image">Image</label>
                                        <input type="file" class="form-control" id="image" name="image" value="{{old('image') ?? $category->image}}">
                                        <img src="{{asset('upload/category/'.$category->image)}}" alt="{{$category->name}}" class="mt-3" width="60" height="60" />
                                        @error('image') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="status">Status</label>
                                        <input type="checkbox" class="form-check" id="status" name="status" @checked($category->status == '1')>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-12 my-3">
                                        <h4>SEO</h4>
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="meta_title">Meta title</label>
                                        <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{old('meta_title') ?? $category->meta_title}}">
                                        @error('meta_title') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-6 form-group mb-3">
                                        <label for="meta_keyword">Meta keyword</label>
                                        <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" value="{{old('meta_keyword') ?? $category->meta_keyword}}">
                                        @error('meta_keyword') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>

                                    <div class="col-12 form-group mb-3">
                                        <label for="meta_description">Meta description</label>
                                        <textarea class="form-control" id="meta_description" name="meta_description" rows="3">{{old('meta_description') ?? $category->meta_description}}</textarea>
                                        @error('meta_description') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">UPDATE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper end -->

        <!-- Start footer -->
        <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
            </div>
        </footer>
        <!-- End footer -->
    </div>
@endsection
