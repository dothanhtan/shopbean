@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
    <!-- content-wrapper start -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="d-flex justify-content-between align-items-center">
                            <span class="text-uppercase">Add Product</span>
                            <a href="{{route('products.index')}}" class="btn btn-outline-secondary">BACK</a>
                        </h3>
                    </div>
                    @if($errors->any())
                        <div class="alert alert-warning">
                            @foreach($errors->all() as $error)
                                <div>{{$error}}</div>
                            @endforeach
                        </div>
                    @endif
                    <form method="POST" enctype="multipart/form-data" action="{{route('products.store')}}">
                        @csrf
                        <div class="card-body">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">HOME</button>
                                    <button class="nav-link" id="nav-seo-tab" data-bs-toggle="tab" data-bs-target="#nav-seo" type="button" role="tab" aria-controls="nav-seo" aria-selected="false">SEO TAG</button>
                                    <button class="nav-link" id="nav-detail-tab" data-bs-toggle="tab" data-bs-target="#nav-detail" type="button" role="tab" aria-controls="nav-detail" aria-selected="false">DETAILS</button>
                                    <button class="nav-link" id="nav-image-tab" data-bs-toggle="tab" data-bs-target="#nav-image" type="button" role="tab" aria-controls="nav-image" aria-selected="false">PRODUCT IMAGE</button>
                                    <button class="nav-link" id="nav-color-tab" data-bs-toggle="tab" data-bs-target="#nav-color" type="button" role="tab" aria-controls="nav-color" aria-selected="false">PRODUCT COLOR</button>
                                    <button class="nav-link" id="nav-size-tab" data-bs-toggle="tab" data-bs-target="#nav-size" type="button" role="tab" aria-controls="nav-size" aria-selected="false">PRODUCT SIZE</button>
                                </div>
                            </nav>

                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="row">
                                        <div class="col-6 form-group my-3">
                                            <label for="category_id">Select Category</label>
                                            <select id="category_id" name="category_id" class="form-control">
                                                <option>--Select Category--</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-6 form-group my-3">
                                            <label for="brand">Select Brand</label>
                                            <select id="brand" name="brand" class="form-control">
                                                <option>--Select Brand--</option>
                                                @foreach($brands as $brand)
                                                    <option value="{{$brand->name}}">{{$brand->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('brand') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-6 form-group mb-3">
                                            <label for="name">Name</label>
                                            <input class="form-control" id="name" name="name" value="{{old('name')}}" autofocus>
                                            @error('name') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-6 form-group mb-3">
                                            <label for="slug">Slug</label>
                                            <input class="form-control" id="slug" name="slug" value="{{old('slug')}}" autofocus>
                                            @error('slug') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-12 form-group mb-3">
                                            <label for="short_description">Short Description</label>
                                            <textarea class="form-control" id="short_description" name="short_description" rows="3">{{old('short_description')}}</textarea>
                                            @error('short_description') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-12 form-group mb-3">
                                            <label for="description">Description</label>
                                            <textarea class="form-control" id="description" name="description" rows="5">{{old('description')}}</textarea>
                                            @error('description') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="nav-seo" role="tabpanel" aria-labelledby="nav-seo-tab">
                                    <div class="row">
                                        <div class="col-6 form-group my-3">
                                            <label for="meta_title">Meta title</label>
                                            <input type="text" class="form-control" id="meta_title" name="meta_title" value="{{old('meta_title')}}">
                                            @error('meta_title') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-6 form-group my-3">
                                            <label for="meta_keyword">Meta keyword</label>
                                            <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" value="{{old('meta_keyword')}}">
                                            @error('meta_keyword') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>

                                        <div class="col-12 form-group mb-3">
                                            <label for="meta_description">Meta description</label>
                                            <textarea class="form-control" id="meta_description" name="meta_description" rows="3">{{old('meta_description')}}</textarea>
                                            @error('meta_description') <span class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="nav-detail" role="tabpanel" aria-labelledby="nav-detail-tab">
                                    <div class="row">
                                        <div class="col-4 form-group my-3">
                                            <label for="original_price">Original Price</label>
                                            <input type="number" class="form-check" id="original_price" name="original_price" min="10" value="{{old('original_price')}}">
                                        </div>
                                        <div class="col-4 form-group my-3">
                                            <label for="selling_price">Selling Price</label>
                                            <input type="number" class="form-check" id="selling_price" name="selling_price" min="10" value="{{old('selling_price')}}">
                                        </div>
                                        <div class="col-4 form-group my-3">
                                            <label for="quantity">Quantity</label>
                                            <input type="number" class="form-check" id="quantity" name="quantity" min="0" value="{{old('quantity')}}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-4 form-group mb-3">
                                            <label for="trending">Trending</label>
                                            <input type="checkbox" class="form-check" id="trending" name="trending" style="width: 30px; height: 30px">
                                        </div>

                                        <div class="col-4 form-group mb-3">
                                            <label for="featured">Featured</label>
                                            <input type="checkbox" class="form-check" id="featured" name="featured" style="width: 30px; height: 30px">
                                        </div>

                                        <div class="col-4 form-group mb-3">
                                            <label for="status">Status</label>
                                            <input type="checkbox" class="form-check" id="status" name="status" style="width: 30px; height: 30px">
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="nav-image" role="tabpanel" aria-labelledby="nav-image-tab">
                                    <div class="col-12 form-group my-3">
                                        <label for="image">Upload Product Image</label>
                                        <input type="file" class="form-control" id="image" name="image[]" multiple value="{{old('image')}}">
                                        @error('image') <span class="text-danger">{{$message}}</span> @enderror
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="nav-color" role="tabpanel" aria-labelledby="nav-color-tab">
                                    <div class="col-12 my-3">
                                        <label>Select Color</label>
                                        <div class="border-bottom mt-2"></div>
                                        <div class="row">
                                            @forelse($colors as $color)
                                                <div class="col-3">
                                                    <div class="border mt-3 p-3">
                                                        <div>
                                                            <label style="float: left; width: 50%">Color</label>
                                                            <span style="float:left; width: 50%;"><input type="checkbox" id="colors" name="colors[{{$color->id}}]" value="{{$color->id}}"> {{$color->name}}</span>
                                                            <div style="clear: both"></div>
                                                        </div>

                                                        <div class="mt-2">
                                                            <label style="float: left; width: 50%; line-height: 25px">Quantity</label>
                                                            <input type="number" name="color_quantity[{{$color->id}}]" style="float:left; width: 50%;">
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="col-12">
                                                    <h3>No colors found</h3>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="nav-size" role="tabpanel" aria-labelledby="nav-size-tab">
                                    <div class="col-12 my-3">
                                        <label>Select Size</label>
                                        <div class="border-bottom mt-2"></div>
                                        <div class="row">
                                            @forelse($sizes as $size)
                                                <div class="col-3">
                                                    <div class="border mt-3 p-3">
                                                        <div>
                                                            <label style="float: left; width: 50%">Size</label>
                                                            <span style="float:left; width: 50%;"><input type="checkbox" id="sizes" name="sizes[{{$size->id}}]" value="{{$size->id}}"> {{$size->name}}</span>
                                                            <div style="clear: both"></div>
                                                        </div>

                                                        <div class="mt-2">
                                                            <label style="float: left; width: 50%; line-height: 25px">Quantity</label>
                                                            <input type="number" name="size_quantity[{{$size->id}}]" style="float:left; width: 50%;">
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @empty
                                                <div class="col-12">
                                                    <h3>No sizes found</h3>
                                                </div>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-outline-primary">SUBMIT</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper end -->

    <!-- Start footer -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
            <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2022
        </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
            <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
        </span>
        </div>
    </footer>
    <!-- End footer -->
</div>
@endsection
