<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h4 class="footer-heading">{{$appSetting->website_name ?? 'Website Name'}}</h4>
                <div class="footer-underline"></div>
                <p style="text-align: justify">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                </p>
            </div>
            <div class="col-md-3">
                <h4 class="footer-heading">Quick Links</h4>
                <div class="footer-underline"></div>
                <div class="mb-2"><a href="{{route('shopping')}}" class="text-white">Home</a></div>
                <div class="mb-2"><a href="" class="text-white">About Us</a></div>
                <div class="mb-2"><a href="" class="text-white">Contact Us</a></div>
                <div class="mb-2"><a href="" class="text-white">Blogs</a></div>
                <div class="mb-2"><a href="" class="text-white">Sitemaps</a></div>
            </div>
            <div class="col-md-3">
                <h4 class="footer-heading">Shop Now</h4>
                <div class="footer-underline"></div>
                <div class="mb-2"><a href="{{route('category')}}" class="text-white">All Categories</a></div>
                <div class="mb-2"><a href="{{route('trending.product')}}" class="text-white">Trending Products</a></div>
                <div class="mb-2"><a href="{{route('new.arrival')}}" class="text-white">New Arrivals</a></div>
                <div class="mb-2"><a href="{{route('featured.product')}}" class="text-white">Featured Products</a></div>
                <div class="mb-2"><a href="{{route('cart')}}" class="text-white">Shopping Cart</a></div>
            </div>
            <div class="col-md-3">
                <h4 class="footer-heading">Reach Us</h4>
                <div class="footer-underline"></div>
                <div class="mb-2">
                    <span><i class="fas fa-map-marker-alt"></i> {{$appSetting->address ?? 'address'}}</span>
                </div>
                <div class="mb-2">
                    <a href="" class="text-white"><i class="fas fa-phone"></i> {{$appSetting->phone1 ?? 'Phone 1'}}</a>
                </div>
                <div class="mb-2">
                    <a href="" class="text-white"><i class="far fa-envelope"></i> {{$appSetting->email1 ?? 'Email 1'}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="copyright-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class=""> &copy; 2022 - {{$appSetting->page_title ?? 'Page Title'}}. All rights reserved.</p>
            </div>
            <div class="col-md-4">
                <div class="social-media">
                    Get Connected:
                    @if($appSetting->facebook)
                    <a href="{{url($appSetting->facebook)}}" target="_blank"><i class="fab fa-facebook"></i></a>
                    @endif

                    @if($appSetting->twitter)
                    <a href="{{$appSetting->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a>
                    @endif

                    @if($appSetting->instagram)
                    <a href="{{$appSetting->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a>
                    @endif

                    @if($appSetting->youtube)
                    <a href="{{$appSetting->youtube}}" target="_blank"><i class="fab fa-youtube"></i></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
