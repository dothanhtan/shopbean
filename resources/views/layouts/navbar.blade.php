<div class="top-navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 my-auto d-none d-sm-none d-md-block d-lg-block">
                <h5 class="brand-name">{{$appSetting->website_name ?? 'Website Name'}}</h5>
            </div>
            <div class="col-md-5 my-auto">
                <form action="{{route('search')}}" method="GET" role="search">
                    <div class="input-group">
                        <input type="search" name="search" value="{{Request::get('search')}}" placeholder="Search your product" class="form-control" />
                        <button class="btn bg-white" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div>
            <div class="col-md-5 my-auto">
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('cart')}}">
                            <i class="fa fa-shopping-cart"></i> Cart (<livewire:shopping.cart.count />)
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{route('wishlist')}}">
                            <i class="fa fa-heart"></i> Wishlist (<livewire:shopping.wishlist.count />)
                        </a>
                    </li>

                    @guest
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}"><i class="fas fa-right-to-bracket"></i> {{ __('Login') }}</a>
                            </li>
                        @endif

                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}"><i class="far fa-registered"></i> {{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user"></i> {{ Auth::user()->name }}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li>
                                    <a class="dropdown-item" href="{{ route('admin.dashboard') }}">
                                        <i class="fas fa-tachometer-alt"></i> {{ __('Dashboard') }}
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('profile.index')}}">
                                        <i class="fa fa-user"></i> {{ __('Profile') }}
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('order.index')}}">
                                        <i class="fa fa-list"></i> {{ __('My Orders') }}
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('wishlist')}}">
                                        <i class="fa fa-heart"></i> {{ __('My Wishlist') }}
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('cart')}}">
                                        <i class="fa fa-shopping-cart"></i> {{ __('My Cart') }}
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-md">
    <div class="container-fluid">
        <a class="navbar-brand d-block d-sm-block d-md-none d-lg-none" href="{{route('shopping')}}">
            Sudo Eco
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{request()->routeIs('shopping') ? 'fw-bold' : ''}}" href="{{route('shopping')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{request()->routeIs('category') ? 'fw-bold' : ''}}" href="{{route('category')}}">All Categories</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{request()->routeIs('trending.product') ? 'fw-bold' : ''}}" href="{{route('trending.product')}}">Trending Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{request()->routeIs('new.arrival') ? 'fw-bold' : ''}}" href="{{route('new.arrival')}}">New Arrivals</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{request()->routeIs('featured.product') ? 'fw-bold' : ''}}" href="{{route('featured.product')}}">Featured Products</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
