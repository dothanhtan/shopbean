<div class="py-3 py-md-4 checkout">
    <div class="container">
        <h4>Checkout</h4>
        <div class="border-bottom my-3"></div>

        @if($this->totalProductPrice != 0)
            <div class="row">
                <div class="col-md-12 mb-4">
                    <div class="shadow bg-white p-3">
                        <h4 class="d-flex justify-content-between text-primary">
                            <span>Item Total Amount :</span><span>${{number_format($this->totalProductPrice, 2)}}</span>
                        </h4>
                        <div class="border-bottom my-3"></div>
                        <small>* Items will be delivered in 3 - 5 days.</small>
                        <small class="d-block">* Tax and other charges are included ?</small>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="shadow bg-white p-3">
                        <h4 class="text-primary">Basic Information</h4>
                        <div class="border-bottom my-3"></div>

                        <form action="" method="POST">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="name">Full Name</label>
                                    <input type="text" id="name" wire:model.defer="name" class="form-control" placeholder="Enter Full Name" />
                                    @error('name')<small class="text-danger">{{$message}}</small>@enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="email">Email Address</label>
                                    <input type="email" id="email" wire:model.defer="email" class="form-control" placeholder="Enter Email Address" />
                                    @error('email')<small class="text-danger">{{$message}}</small>@enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="phone">Phone Number</label>
                                    <input type="number" id="phone" wire:model.defer="phone" class="form-control" placeholder="Enter Phone Number" />
                                    @error('phone')<small class="text-danger">{{$message}}</small>@enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="pin_code">Pin-code (Zip-code)</label>
                                    <input type="number" id="pin_code" wire:model.defer="pin_code" class="form-control" placeholder="Enter Pin-code" />
                                    @error('pin_code')<small class="text-danger">{{$message}}</small>@enderror
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="address">Address</label>
                                    <textarea id="address" wire:model.defer="address" class="form-control" rows="2"></textarea>
                                    @error('address')<small class="text-danger">{{$message}}</small>@enderror
                                </div>
                                <div class="col-md-12 mb-3" wire:ignore>
                                    <label>Select Payment Mode: </label>
                                    <div class="d-md-flex align-items-start">
                                        <div class="nav col-md-3 flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <button class="nav-link active fw-bold" wire:loading.attr="disabled" id="cashOnDeliveryTab-tab" data-bs-toggle="pill" data-bs-target="#cashOnDeliveryTab" type="button" role="tab" aria-controls="cashOnDeliveryTab" aria-selected="true">Cash on Delivery</button>
                                            <button class="nav-link fw-bold" wire:loading.attr="disabled" id="onlinePayment-tab" data-bs-toggle="pill" data-bs-target="#onlinePayment" type="button" role="tab" aria-controls="onlinePayment" aria-selected="false">Online Payment</button>
                                        </div>
                                        <div class="tab-content col-md-9" id="v-pills-tabContent">
                                            <div class="tab-pane active show fade" id="cashOnDeliveryTab" role="tabpanel" aria-labelledby="cashOnDeliveryTab-tab" tabindex="0">
                                                <h6>Cash on Delivery Mode</h6>
                                                <div class="border-bottom my-3"></div>
                                                <button type="button" wire:loading.attr="disabled" wire:click="codOrder" class="btn btn-primary">
                                                    <span wire:loading.remove wire:target="codOrder">Place Order (Cash on Delivery)</span>
                                                    <span wire:loading wire:target="codOrder">Place Order</span>
                                                </button>
                                            </div>
                                            <div class="tab-pane fade" id="onlinePayment" role="tabpanel" aria-labelledby="onlinePayment-tab" tabindex="0">
                                                <h6>Online Payment Mode</h6>
                                                <div class="border-bottom my-3"></div>
{{--                                                <button type="button" wire:loading.attr="disabled" class="btn btn-warning">Pay Now (Online Payment)</button>--}}
                                                <div><div id="paypal-button-container"></div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @else
            <div class="card card-body shadow text-center p-md-5">
                <h4 class="mb-4">No item in cart to checkout</h4>
                <a href="{{route('banner')}}" class="btn btn-warning"><strong>Shop Now</strong></a>
            </div>
        @endif
    </div>
</div>

@push('script')
    <!-- Replace "test" with your own sandbox Business account app client ID -->
    <script src="https://www.paypal.com/sdk/js?client-id=AQPzhjJ96v603yK9DF4qhll_FB0EzJnLUEG_dK0yrMisBUjs7VxOKKfb2HXN6ORSpLLar1k40rTi9vm9&currency=USD"></script>
    <script>
        paypal.Buttons({
            onClick: function () {
                let name = !document.getElementById('name').value
                let email = !document.getElementById('email').value
                let phone = !document.getElementById('phone').value
                let pin_code = !document.getElementById('pin_code').value
                let address = !document.getElementById('address').value
                if (name || email || phone || pin_code || address) {
                    Livewire.emit('validation')
                    return false
                } else {
                    @this.set('name', document.getElementById('name').value)
                    @this.set('email', document.getElementById('email').value)
                    @this.set('phone', document.getElementById('phone').value)
                    @this.set('pin_code', document.getElementById('pin_code').value)
                    @this.set('address', document.getElementById('address').value)
                }
            },
            // Sets up the transaction when a payment button is clicked
            createOrder: (data, actions) => {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: "{{$this->totalProductPrice}}" // Can also reference a variable or function
                        }
                    }]
                });
            },
            // Finalize the transaction after payer approval
            onApprove: (data, actions) => {
                return actions.order.capture().then(function(orderData) {
                    // Successful capture! For dev/demo purposes:
                    console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
                    const transaction = orderData.purchase_units[0].payments.captures[0];
                    if (transaction.status == "COMPLETED") {
                        Livewire.emit('transaction', transaction.id)
                    }
                    // alert(`Transaction ${transaction.status}: ${transaction.id}\n\nSee console for all available details`);
                    // When ready to go live, remove the alert and show a success message within this page. For example:
                    // const element = document.getElementById('paypal-button-container');
                    // element.innerHTML = '<h3>Thank you for your payment!</h3>';
                    // Or go to another URL:  actions.redirect('thank_you.html');
                });
            }
        }).render('#paypal-button-container');
    </script>
@endpush
