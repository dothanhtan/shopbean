<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-header bg-info">
                <h4>Filter by brand</h4>
            </div>
            <div class="card-body">
                @foreach($category->brands as $brandItem)
                    <div class="form-check">
                        <input type="checkbox" wire:model="brandInput" name="filterName" id="filterBrand{{$brandItem->id}}" class="form-check-input" value="{{$brandItem->name}}">
                        <label for="filterBrand{{$brandItem->id}}" class="form-check-label">{{$brandItem->name}}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-header bg-secondary">
                <h4>Filter by price</h4>
            </div>
            <div class="card-body">
                <div class="form-check">
                    <input type="radio" wire:model="priceInput" name="sortPrice" id="lowest" class="form-check-input" value="low-to-high">
                    <label for="lowest" class="form-check-label">Low to High</label>
                </div>
                <div class="form-check">
                    <input type="radio" wire:model="priceInput" name="sortPrice" id="highest" class="form-check-input" value="high-to-low">
                    <label for="highest" class="form-check-label">High to Low</label>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="row">
            @forelse($products as $product)
                <div class="col-md-3">
                    <div class="product-card">
                        <div class="product-card-img">
                            @if($product->quantity > 0)
                                <label class="stock bg-success">In Stock</label>
                            @else
                                <label class="stock bg-danger">Out Of Stock</label>
                            @endif

                            @if($product->productImages->count() > 0)
                                <a href="{{route('product.detail', [$product->category->slug, $product->slug])}}">
                                    <img src="{{asset($product->productImages[0]->image)}}" alt="{{$product->name}}">
                                </a>
                            @else
                                <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                            @endif
                        </div>
                        <div class="product-card-body">
                            <p class="product-brand">
                                {{$product->brand}}
                            </p>
                            <h5 class="product-name">
                                <a href="{{route('product.detail',[$product->category->slug, $product->slug])}}">
                                    {{$product->name}}
                                </a>
                            </h5>
                            <div>
                                <span class="selling-price">${{$product->selling_price}}</span>
                                <span class="original-price">${{$product->original_price}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12">
                    <h3>No products available for {{$category->name}}</h3>
                </div>
            @endforelse
        </div>
    </div>
</div>
