<div class="py-3 py-md-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-5 mt-3">
                <div class="bg-white border" wire:ignore>
                    @if($product->productImages)
                        <div class="exzoom" id="exzoom">
                            <div class="exzoom_img_box">
                                <ul class='exzoom_img_ul'>
                                    @foreach($product->productImages as $images)
                                        <li><img src="{{asset($images->image)}}" alt="Image"/></li>
                                    @endforeach
                                </ul>
                            </div>

                            <div class="exzoom_nav"></div>

                            <p class="exzoom_btn">
                                <a href="javascript:void(0);" class="exzoom_prev_btn"> <i class="fas fa-chevron-left"></i> </a>
                                <a href="javascript:void(0);" class="exzoom_next_btn"> <i class="fas fa-chevron-right"></i> </a>
                            </p>
                        </div>
                    @else
                        <img src="{{asset('upload/product/default.jpg')}}" class="w-100" alt="Img">
                    @endif
                </div>
            </div>
            <div class="col-md-7 mt-3">
                <div class="product-view">
                    <h4 class="product-name">
                        {{$product->name}}
                    </h4>
                    <hr>
                    <p class="product-path">
                        Home / {{$product->category->name}} / {{$product->name}}
                    </p>
                    <div>
                        <span class="selling-price">${{$product->selling_price}}</span>
                        <span class="original-price">${{$product->original_price}}</span>
                    </div>

                    @if($product->productColors and $product->productColors->count() > 0)
                        <div class="mt-2">
                            <strong class="me-2">Color:</strong>
                            @foreach($product->productColors as $itemColor)
                                <label class="colorChoice" wire:click="colorSelected({{$itemColor->id}})" style="background-color: {{$itemColor->color->code}}">
                                    <strong>{{$itemColor->color->name}}</strong>
                                </label>
                            @endforeach

                            @if($this->productColorQuantity == 'outOfStock')
                                <label class="btn-sm text-white text-uppercase bg-danger"><strong> Out Of Stock </strong></label>
                            @elseif($this->productColorQuantity > 0)
                                <label class="btn-sm text-white text-uppercase bg-success"><strong> In Stock </strong></label>
                            @endif
                        </div>
                    @endif

                    @if($product->productSizes and $product->productSizes->count() > 0)
                        <div class="mt-2">
                            <strong class="me-2">Size:</strong>
                            @foreach($product->productSizes as $itemSize)
                                <label class="sizeChoice" wire:click="sizeSelected({{$itemSize->id}})">
                                    <strong>{{$itemSize->size->sign}}</strong>
                                </label>
                            @endforeach

                            @if($this->productSizeQuantity == 'outOfStock')
                                <label class="btn-sm text-white text-uppercase bg-danger"><strong> Out Of Stock </strong></label>
                            @elseif($this->productSizeQuantity > 0)
                                <label class="btn-sm text-white text-uppercase bg-success"><strong> In Stock </strong></label>
                            @endif
                        </div>
                    @endif

                    @if($product->quantity == 0)
                        <div class="py-1 mt-2">
                            <label class="btn-sm text-white text-uppercase bg-danger"><strong> Out Of Stock </strong></label>
                        </div>
                    @endif

                    <div class="mt-2">
                        <div class="input-group">
                            <span class="btn btn1" wire:click="decrementQuantity()"><i class="fa fa-minus"></i></span>
                            <input type="text" wire:model="quantityCount" value="{{$this->quantityCount}}" class="input-quantity" readonly/>
                            <span class="btn btn1" wire:click="incrementQuantity()"><i class="fa fa-plus"></i></span>
                        </div>
                    </div>
                    <div class="mt-2">
                        <button type="button" wire:click="addToCart({{$product->id}})" class="btn btn1">
                            <i class="fa fa-shopping-cart"></i> Add To Cart
                        </button>
                        <button type="button" wire:click="addToWishlist({{$product->id}})" class="btn btn1">
                            <span wire:loading.remove wire:target="addToWishlist"><i class="fa fa-heart"></i> Add To Wishlist</span>
                            <span wire:loading wire:target="addToWishlist">Adding...</span>
                        </button>
                    </div>
                    <div class="mt-3">
                        <h5 class="mb-0">Small Description</h5>
                        <p>{{$product->short_description}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-3">
                <div class="card">
                    <div class="card-header bg-white">
                        <h4>Description</h4>
                    </div>
                    <div class="card-body">
                        <p>{{$product->description}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="py-3 py-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h4>Related @if($category) {{$category->name}} @endif Product</h4>
                <div class="underline mb-4"></div>
            </div>

            <div class="col-md-12">
                @if($category)
                    <div class="owl-carousel owl-theme product-carousel">
                        @foreach($category->relatedProducts as $relatedItem)
                            <div class="item mb-3">
                                <div class="product-card">
                                    <div class="product-card-img">
                                        @if($relatedItem->productImages->count() > 0)
                                            <a href="{{route('product.detail', [$relatedItem->category->slug, $relatedItem->slug])}}">
                                                <img src="{{asset($relatedItem->productImages[0]->image)}}" alt="{{$relatedItem->name}}">
                                            </a>
                                        @else
                                            <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                                        @endif
                                    </div>
                                    <div class="product-card-body">
                                        <p class="product-brand">
                                            {{$relatedItem->brand}}
                                        </p>
                                        <h5 class="product-name">
                                            <a href="{{route('product.detail',[$relatedItem->category->slug, $relatedItem->slug])}}">
                                                {{$relatedItem->name}}
                                            </a>
                                        </h5>
                                        <div>
                                            <span class="selling-price">${{$relatedItem->selling_price}}</span>
                                            <span class="original-price">${{$relatedItem->original_price}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="p-2">
                        <h3>No related products available</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="py-3 py-md-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-3">
                <h4>Related @if($product) {{$product->brand}} @endif Product</h4>
                <div class="underline mb-4"></div>
            </div>

            <div class="col-md-12">
                @if($category)
                    <div class="owl-carousel owl-theme product-carousel">
                        @foreach($category->relatedProducts as $relatedItem)
                            @if($relatedItem->brand == "$product->brand")
                                <div class="item mb-3">
                                    <div class="product-card">
                                        <div class="product-card-img">
                                            @if($relatedItem->productImages->count() > 0)
                                                <a href="{{route('product.detail', [$relatedItem->category->slug, $relatedItem->slug])}}">
                                                    <img src="{{asset($product->productImages[0]->image)}}" alt="{{$product->name}}">
                                                </a>
                                            @else
                                                <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                                            @endif
                                        </div>
                                        <div class="product-card-body">
                                            <p class="product-brand">
                                                {{$relatedItem->brand}}
                                            </p>
                                            <h5 class="product-name">
                                                <a href="{{route('product.detail',[$relatedItem->category->slug, $relatedItem->slug])}}">
                                                    {{$relatedItem->name}}
                                                </a>
                                            </h5>
                                            <div>
                                                <span class="selling-price">${{$relatedItem->selling_price}}</span>
                                                <span class="original-price">${{$relatedItem->original_price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @else
                    <div class="p-2">
                        <h3>No related products available</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        $(function(){
            $("#exzoom").exzoom({
                "navWidth": 60,
                "navHeight": 60,
                "navItemNum": 5,
                "navItemMargin": 7,
                "navBorder": 1,
                "autoPlay": true,
                "autoPlayTimeout": 2000
            });
        });

        $('.product-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })
    </script>
@endpush
