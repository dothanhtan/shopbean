<div class="py-3 py-md-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>My Cart</h3>
                <div class="border-bottom mt-2 mb-4"></div>
            </div>
            <div class="col-md-12">
                <div class="shopping-cart">
                    <div class="cart-header d-none d-sm-none d-mb-block d-lg-block">
                        <div class="row">
                            <div class="col-md-6">
                                <h4>Products</h4>
                            </div>
                            <div class="col-md-1">
                                <h4>Price</h4>
                            </div>
                            <div class="col-md-2">
                                <h4>Quantity</h4>
                            </div>
                            <div class="col-md-1">
                                <h4>Total</h4>
                            </div>
                            <div class="col-md-2">
                                <h4>Remove</h4>
                            </div>
                        </div>
                    </div>
                    @forelse($cart as $item)
                        <div class="cart-item">
                            <div class="row">
                                <div class="col-md-6 my-auto">
                                    <a href="{{route('product.detail', [$item->product->category->slug, $item->product->slug])}}">
                                        <label class="product-name">
                                            <img src="{{asset($item->product->productImages[0]->image)}}" style="width: 50px; height: 50px" alt="{{$item->product->name}}">
                                            <span>{{$item->product->name}}</span>
                                            @if($item->productColor and $item->productColor->color)
                                                <span> - Color: {{$item->productColor->color->name}}</span>
                                            @endif
                                            @if($item->productSize and $item->productSize->size)
                                                <span> - Size: {{$item->productSize->size->sign}}</span>
                                            @endif
                                        </label>
                                    </a>
                                </div>
                                <div class="col-md-1 my-auto">
                                    <label class="price">${{$item->product->selling_price}} </label>
                                </div>
                                <div class="col-md-2 col-7 my-auto">
                                    <div class="quantity">
                                        <div class="input-group">
                                            <button type="button" wire:loading.attr="disabled" wire:click="decrementQuantity({{$item->id}})" class="btn btn1"><i class="fa fa-minus"></i></button>
                                            <input type="text" value="{{$item->quantity}}" class="input-quantity" />
                                            <button type="button" wire:loading.attr="disabled" wire:click="incrementQuantity({{$item->id}})" class="btn btn1"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 my-auto">
                                    @php($total = $item->product->selling_price * $item->quantity)
                                    <label class="price">${{number_format($total, 2)}} </label>
                                    @php($totalPrice += $item->product->selling_price * $item->quantity)
                                </div>
                                <div class="col-md-2 col-5 my-auto">
                                    <div class="remove">
                                        <button type="button" wire:loading.attr="disabled" wire:click="removeCartItem({{$item->id}})" class="btn btn-danger btn-sm">
                                            <span wire:loading.remove wire:target="removeCartItem({{$item->id}})">
                                                <i class="fa fa-trash"></i> Remove
                                            </span>
                                            <span wire:loading wire:target="removeCartItem({{$item->id}})">
                                                <i class="fa fa-trash"></i> Removing
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h4>No cart item available</h4>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mt-3 my-auto">
                <h5 class="mt-5">Get the best deals & offers <a href="{{route('shopping')}}">Shop Now</a></h5>
            </div>
            <div class="col-md-4 mt-3">
                <div class="shadow-sm bg-white p-3">
                    <h4 class="border-bottom pb-2 d-flex justify-content-between"><span>Total:</span><span>${{number_format($totalPrice, 2)}}</span></h4>
                    <a href="{{route('checkout')}}" class="btn btn-block btn-warning w-100"><strong>Checkout</strong></a>
                </div>
            </div>
        </div>
    </div>
</div>
