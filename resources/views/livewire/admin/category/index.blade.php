<div class="main-panel">
    <!-- content-wrapper start -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="d-flex justify-content-between align-items-center mb-0">
                            <span class="text-uppercase fw-bold">Category</span>
                            <a href="{{route('categories.create')}}" class="btn btn-sm btn-outline-primary">Add Category</a>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                        <tr>
                                            <td>
                                                {{$category->id}}
                                            </td>
                                            <td>
                                                <img src="{{asset('upload/category/'.$category->image)}}" alt="{{$category->name}}" width="150" height="150">
                                            </td>
                                            <td>
                                                {{$category->name}}
                                            </td>
                                            <td>
                                                {{$category->slug}}
                                            </td>
                                            <td>
                                                <span class="badge bg-primary">{{$category->status == 1 ? 'hidden' : 'visible'}}</span>
                                            </td>
                                            <td>
                                                <a href="{{route('categories.edit', $category->id)}}" class="btn btn-sm btn-outline-warning"><i class="mdi mdi-pencil"></i></a>
                                                <a href="#" wire:click="deleteCategory({{$category->id}})" class="btn btn-sm btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deleteModal"><i class="mdi mdi-delete"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-4">
                            {{$categories->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('livewire.admin.category.modal')
    </div>
    <!-- content-wrapper end -->

    <!-- Start footer -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
        </div>
    </footer>
    <!-- End footer -->
</div>

@push('script')
    <script>
        window.addEventListener('close-modal', event => {
            $('#deleteModal').modal('hide');
        });
    </script>
@endpush
