<div class="main-panel">
    <!-- content-wrapper start -->
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="d-flex justify-content-between align-items-center mb-0">
                            <span class="text-uppercase fw-bold">Brand</span>
                            <a href="#" class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#addBrandModal">Add Brand</a>
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($brands as $brand)
                                    <tr>
                                        <td>
                                            {{$brand->id}}
                                        </td>
                                        <td>
                                            {{$brand->category ? $brand->category->name : 'No category'}}
                                        </td>
                                        <td>
                                            {{$brand->name}}
                                        </td>
                                        <td>
                                            {{$brand->slug}}
                                        </td>
                                        <td>
                                            <span class="badge bg-primary">{{$brand->status == 1 ? 'hidden' : 'visible'}}</span>
                                        </td>
                                        <td>
                                            <a href="#" wire:click="editBrand({{$brand->id}})" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#editBrandModal"><i class="mdi mdi-pencil"></i></a>
                                            <a href="#" wire:click="deleteBrand({{$brand->id}})" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#deleteBrandModal"><i class="mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">No Brands Found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-4">
                            {{$brands->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('livewire.admin.brand.modal')
    </div>
    <!-- content-wrapper end -->

    <!-- Start footer -->
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright ©
                <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021
            </span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best
                <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard  </a> templates
            </span>
        </div>
    </footer>
    <!-- End footer -->
</div>

@push('script')
    <script>
        window.addEventListener('close-modal', event => {
            $('#addBrandModal').modal('hide');
            $('#editBrandModal').modal('hide');
            $('#deleteBrandModal').modal('hide');
        });
    </script>
@endpush
