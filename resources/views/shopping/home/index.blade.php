@extends('layouts.app')
@section('title', 'Home Page')
@section('content')
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            @foreach($banners as $key => $banner)
                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                    <img src="{{asset("upload/banner/$banner->image")}}" class="d-block w-100" alt="{{$banner->image}}">
                    <div class="carousel-caption d-none d-md-block">
                        <div class="custom-carousel-content">
                            <h1>{!! $banner->title !!}</h1>
                            <p>{!! $banner->description !!}</p>
                            <div><a href="#" class="btn btn-slider">Get Now</a></div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="py-5 bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    <h4>Welcome to {{$appSetting->website_name ?? 'Website Name'}}</h4>
                    <div class="underline mx-auto"></div>
                    <p class="text-justify">
                        Lorem ipsum dolor sit amet, consecrate disciplining elite,
                        sed do usermod temper incident ut labor et color magna aliquot.
                        Ut enum ad minim venial, quits nostrum excitation labors nisei ut
                        aliquot ex ea common consequent. Dis auto inure dolor in reprehend
                        in voluptuary valid ease cilium color eu fugit null paginator.
                        Except saint toccata cupidity non president, sunt in gulp
                        qui official underused moll-it anim id est labor.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="d-flex justify-content-between">
                        <span>Trending Products</span>
                        <a href="{{route('trending.product')}}" class="btn btn-info">View More</a>
                    </h4>
                    <div class="underline mb-4"></div>
                </div>
                @if($trendingProducts)
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme product-carousel">
                            @foreach($trendingProducts as $product)
                                <div class="item">
                                    <div class="product-card">
                                        <div class="product-card-img">
                                            <label class="stock bg-success">New</label>

                                            @if($product->productImages->count() > 0)
                                                <a href="{{route('product.detail', [$product->category->slug, $product->slug])}}">
                                                    <img src="{{asset($product->productImages[0]->image)}}" alt="{{$product->name}}">
                                                </a>
                                            @else
                                                <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                                            @endif
                                        </div>
                                        <div class="product-card-body">
                                            <p class="product-brand">
                                                {{$product->brand}}
                                            </p>
                                            <h5 class="product-name">
                                                <a href="{{route('product.detail',[$product->category->slug, $product->slug])}}">
                                                    {{$product->name}}
                                                </a>
                                            </h5>
                                            <div>
                                                <span class="selling-price">${{$product->selling_price}}</span>
                                                <span class="original-price">${{$product->original_price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <h3>No trending products available</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="d-flex justify-content-between">
                        <span>New Arrivals</span>
                        <a href="{{route('new.arrival')}}" class="btn btn-info">View More</a>
                    </h4>
                    <div class="underline mb-4"></div>
                </div>
                @if($newArrivalProducts)
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme product-carousel">
                            @foreach($newArrivalProducts as $product)
                                <div class="item">
                                    <div class="product-card">
                                        <div class="product-card-img">
                                            <label class="stock bg-success">New</label>

                                            @if($product->productImages->count() > 0)
                                                <a href="{{route('product.detail', [$product->category->slug, $product->slug])}}">
                                                    <img src="{{asset($product->productImages[0]->image)}}" alt="{{$product->name}}">
                                                </a>
                                            @else
                                                <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                                            @endif
                                        </div>
                                        <div class="product-card-body">
                                            <p class="product-brand">
                                                {{$product->brand}}
                                            </p>
                                            <h5 class="product-name">
                                                <a href="{{route('product.detail',[$product->category->slug, $product->slug])}}">
                                                    {{$product->name}}
                                                </a>
                                            </h5>
                                            <div>
                                                <span class="selling-price">${{$product->selling_price}}</span>
                                                <span class="original-price">${{$product->original_price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <h3>No new arrivals products available</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="d-flex justify-content-between">
                        <span>Featured Products</span>
                        <a href="{{route('featured.product')}}" class="btn btn-info">View More</a>
                    </h4>
                    <div class="underline mb-4"></div>
                </div>
                @if($featuredProducts)
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme product-carousel">
                            @foreach($featuredProducts as $product)
                                <div class="item">
                                    <div class="product-card">
                                        <div class="product-card-img">
                                            <label class="stock bg-success">New</label>

                                            @if($product->productImages->count() > 0)
                                                <a href="{{route('product.detail', [$product->category->slug, $product->slug])}}">
                                                    <img src="{{asset($product->productImages[0]->image)}}" alt="{{$product->name}}">
                                                </a>
                                            @else
                                                <img src="{{asset('upload/product/default.jpg')}}" alt="Coming Soon">
                                            @endif
                                        </div>
                                        <div class="product-card-body">
                                            <p class="product-brand">
                                                {{$product->brand}}
                                            </p>
                                            <h5 class="product-name">
                                                <a href="{{route('product.detail',[$product->category->slug, $product->slug])}}">
                                                    {{$product->name}}
                                                </a>
                                            </h5>
                                            <div>
                                                <span class="selling-price">${{$product->selling_price}}</span>
                                                <span class="original-price">${{$product->original_price}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <h3>No featured products available</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.product-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            dots: false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:4
                }
            }
        })
    </script>
@endsection
