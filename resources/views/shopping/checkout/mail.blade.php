<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Invoice #{{$order->id}}</title>
    <link rel="icon" href="https://www.freeiconspng.com/uploads/pdf-icon-png-16x16-pictures-26.png" type="image/png" sizes="16x16">

    <style>
        html, body {
            margin: 10px;
            padding: 10px;
            font-family: sans-serif;
        }
        h1, h2, h3, h4, h5, h6, p, span, label {
            font-family: sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 0 !important;
        }
        table thead th {
            height: 28px;
            text-align: left;
            font-size: 16px;
            font-family: sans-serif;
        }
        table, th, td {
            border: 1px solid #ddd;
            padding: 8px;
            font-size: 14px;
        }
        .heading {
            font-size: 24px;
            margin-top: 12px;
            margin-bottom: 12px;
            font-family: sans-serif;
        }
        .small-heading {
            font-size: 18px;
            font-family: sans-serif;
        }
        .total-heading {
            font-size: 18px;
            font-weight: 700;
            font-family: sans-serif;
        }
        .order-details tbody tr td:nth-child(1) {
            width: 20%;
        }
        .order-details tbody tr td:nth-child(3) {
            width: 20%;
        }
        .text-start {
            text-align: left;
        }
        .text-end {
            text-align: right;
        }
        .text-center {
            text-align: center;
        }
        .company-data span {
            margin-bottom: 4px;
            display: inline-block;
            font-family: sans-serif;
            font-size: 14px;
            font-weight: 400;
        }
        .no-border {
            border: 1px solid #fff !important;
        }
        .bg-blue {
            background-color: #414ab1;
            color: #fff;
        }
        .mt-4 {
            margin-top: 2rem;
        }
        .mb-3 {
            margin-bottom: 1.5rem;
        }
        .mb-0 {
            margin-bottom: 0;
        }
    </style>
</head>
<body>
    <div class="text-center">
        <h1 class="mb-3">Thank you for your order</h1>
        <p class="mb-0">Thank you for purchasing with {{$appSetting->website_name ?? 'FreshBean Ecommerce'}}</p>
        <p>Your order items and details are provided below</p>
    </div>

    <table class="order-details">
        <thead>
        <tr>
            <th style="width: 50%;" colspan="2">
                <h2 class="text-start">{{$appSetting->website_name}}</h2>
            </th>
            <th style="width: 50%;" colspan="2" class="text-end company-data">
                <span>Invoice Id: #{{$order->id}}</span> <br>
                <span>Date: {{date('d / m / Y')}}</span> <br>
                <span>Email : {{$appSetting->email1}}</span> <br>
                <span>Address: {{$appSetting->address}}</span> <br>
            </th>
        </tr>
        <tr class="bg-blue">
            <th style="width: 50%;" colspan="2">Order Details</th>
            <th style="width: 50%;" colspan="2">User Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Order Id:</td>
            <td>{{$order->id}}</td>

            <td>Full Name:</td>
            <td>{{$order->name}}</td>
        </tr>
        <tr>
            <td>Tracking Id/No.:</td>
            <td>{{$order->tracking_no}}</td>

            <td>Email:</td>
            <td>{{$order->email}}</td>
        </tr>
        <tr>
            <td>Ordered Date:</td>
            <td>{{$order->created_at->format('d-m-Y H:i A')}}</td>

            <td>Phone:</td>
            <td>{{$order->phone}}</td>
        </tr>
        <tr>
            <td>Payment Mode:</td>
            <td>{{$order->payment_mode}}</td>

            <td>Address:</td>
            <td>{{$order->address}}</td>
        </tr>
        <tr>
            <td>Order Status:</td>
            <td>{{$order->status_message}}</td>

            <td>Pin code:</td>
            <td>{{$order->pin_code}}</td>
        </tr>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th class="no-border text-start heading" colspan="5">Order Items</th>
            </tr>
            <tr class="bg-blue">
                <th>ID</th>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>
        @php($totalPrice = 0)
        <tbody>
        @foreach($order->orderItems as $item)
            <tr>
                <td style="width: 10%" class="align-middle">
                    {{$item->id}}
                </td>
                <td class="align-middle">
                    {{$item->product->name}}
                    @if($item->productColor and $item->productColor->color)
                        <span> - Color: {{$item->productColor->color->name}}</span>
                    @endif
                    @if($item->productSize and $item->productSize->size)
                        <span> - Size: {{$item->productSize->size->sign}}</span>
                    @endif
                </td>
                <td style="width: 10%" class="align-middle">
                    ${{$item->price}}
                </td>
                <td style="width: 10%" class="align-middle">
                    {{$item->quantity}}
                </td>
                <td style="width: 15%" class="align-middle fw-bold">
                    ${{number_format($item->price * $item->quantity, 2)}}
                </td>
                @php($totalPrice += $item->price * $item->quantity)
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4" class="total-heading">Total Amount - <small>Inc. all vat/tax</small> :</td>
            <td colspan="1" class="total-heading">${{number_format($totalPrice, 2)}}</td>
        </tr>
        </tfoot>
    </table>

    <p class="text-center mt-4">
        Thank you for shopping with {{$appSetting->website_name}}
    </p>
</body>

</html>
