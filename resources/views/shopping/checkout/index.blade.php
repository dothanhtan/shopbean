@extends('layouts.app')
@section('title', 'Checkout')
@section('content')
    <livewire:shopping.checkout.index />
@endsection
