@extends('layouts.app')

@section('title', 'Thank you')

@section('content')
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12">
                @if(session('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{session('message')}}</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
            <div class="col-md-12">
                <div class="p-4 shadow bg-light text-center">
                    <h2>Your Logo</h2>
                    <h4>Thank you for shopping with Golden Ecommerce</h4>
                    <a href="{{route('shopping')}}" class="btn btn-lg btn-primary mt-4">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
@endsection
