@extends('layouts.app')

@section('title', 'Show Order Detail')

@section('content')
    <div class="py-3 py-md-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="shadow bg-white p-3">
                        <h4 class="d-flex justify-content-between">
                            <span class="text-primary"><i class="fas fa-shopping-cart text-dark"></i> My Order Detail</span>
                            <a href="{{route('order.index')}}" class="btn btn-outline-secondary">Back</a>
                        </h4>
                        <div class="border-bottom my-3"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>Order Detail</h5>
                                <div class="border-bottom my-3"></div>

                                <h6>Tracking ID/NO: {{$order->tracking_no}}</h6>
                                <h6>Ordered Date: {{$order->created_at->format('d-m-Y H:i A')}}</h6>
                                <h6>Payment Mode: {{$order->payment_mode}}</h6>
                                <h6 class="border p-2 text-success">Status Message: <span class="text-uppercase">{{$order->status_message}}</span></h6>
                            </div>

                            <div class="col-md-6">
                                <h5>User Detail</h5>
                                <div class="border-bottom my-3"></div>

                                <h6>Full Name: {{$order->name}}</h6>
                                <h6>Email: {{$order->email}}</h6>
                                <h6>Phone Number: {{$order->phone}}</h6>
                                <h6>Address: {{$order->address}}</h6>
                                <h6>Pin Code: {{$order->pin_code}}</h6>
                            </div>

                            <div class="col-md-12 mt-4">
                                <h5>Item Detail</h5>
                                <div class="border-bottom my-3"></div>

                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Item ID</th>
                                                <th>Image</th>
                                                <th>Product</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        @php($totalPrice = 0)
                                        <tbody>
                                            @foreach($order->orderItems as $item)
                                                <tr>
                                                    <td style="width: 10%" class="align-middle">
                                                        {{$item->id}}
                                                    </td>
                                                    <td style="width: 10%">
                                                        <img src="{{asset($item->product->productImages[0]->image)}}" style="width: 50px; height: 50px" alt="{{$item->product->name}}">
                                                    </td>
                                                    <td class="align-middle">
                                                        {{$item->product->name}}
                                                        @if($item->productColor and $item->productColor->color)
                                                            <span> - Color: {{$item->productColor->color->name}}</span>
                                                        @endif
                                                        @if($item->productSize and $item->productSize->size)
                                                            <span> - Size: {{$item->productSize->size->sign}}</span>
                                                        @endif
                                                    </td>
                                                    <td style="width: 10%" class="align-middle">
                                                        ${{$item->price}}
                                                    </td>
                                                    <td style="width: 10%" class="align-middle">
                                                        {{$item->quantity}}
                                                    </td>
                                                    <td style="width: 10%" class="align-middle fw-bold">
                                                        ${{number_format($item->price * $item->quantity, 2)}}
                                                    </td>
                                                    @php($totalPrice += $item->price * $item->quantity)
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="5" class="fw-bold">Total Amount:</td>
                                                <td colspan="1" class="fw-bold">${{number_format($totalPrice, 2)}}</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
