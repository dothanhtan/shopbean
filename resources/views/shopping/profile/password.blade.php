@extends('layouts.app')

@section('title', 'User Profile')

@section('content')
    <div class="py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @if(session('message'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <strong>{{session('message')}}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if($errors->any())
                        <ul class="alert alert-warning" style="padding-left: 2rem">
                            @foreach($errors->all() as $error)
                                <li class="text-warning">{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="card shadow">
                        <div class="card-header bg-primary">
                            <h4 class="d-flex justify-content-between align-items-center mb-0">
                                <span class="fw-bold">Change Password</span>
                                <a href="{{route('profile.index')}}" class="btn btn-secondary">Back</a>
                            </h4>
                        </div>
                        <form action="{{route('password.change')}}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label for="current_password" class="mb-1">Current Password</label>
                                    <input type="password" id="current_password" name="current_password" class="form-control" autocomplete="current-password">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="password" class="mb-1">New Password</label>
                                    <input type="password" id="password" name="password" class="form-control" autocomplete="new-password">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="re_password" class="mb-1">Confirm Password</label>
                                    <input type="password" id="re_password" name="password_confirmation" class="form-control" autocomplete="new-password">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-outline-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
